"""
Module ini berfungsi untuk menyediakan utilitas yang mendukung untuk proses training, serta menyimpan hasil prediksi

"""

from nltk.tokenize import word_tokenize
from nltk.stem import PorterStemmer
from nltk.corpus import stopwords as nltk_stopwords
from Sastrawi.StopWordRemover.StopWordRemoverFactory import StopWordRemoverFactory
import pandas as pd
import re
import os
import numpy as np
import string
import sys



def rm_unimportant_words(words):
    """
    Menghilangkan kata yang tidak relevan terhadap aspek aspek yang ada

    Keyword arguments:
    words -- list of word
    """ 

    not_important_words = ['kak','untuk','ke','nya','yg','jadi','tapi','aja','deh','ini','juga','jg','dan','utk','krn','yang','untuk','jd','text','gue','gw',
'karna','di','ya','karna','aku','waktu','saya','kayak','coba','kalo','sama','kalau','emang','lg','lagi','tp','udah','dan','buat','pa','li',
'amp','dll','blogspot','lol','ps','oh','man','hehe','haha','hahaha','hehehe']
    
    words = [word for word in words if word not in not_important_words]

    
        
    words = " ".join(words)
    words = words.replace('nya','')
    words = words.replace(' bgt ',' banget ')
    words = words.replace('ambience','suasana')
    words = words.replace('decor','suasana')
    words = words.replace('interior','suasana')
    
    return words
    
def rm_stopwords(sentence):
    """
    Menghilangkan stopwords

    Keyword arguments:
    sentences -- teks atau kalimat
    """ 

    factory = StopWordRemoverFactory()
    stopword = factory.create_stop_word_remover()
    words = stopword.remove(sentence)

    stop_words_set = set(nltk_stopwords.words('english'))
    tokens = word_tokenize(words)
    
    words = [word.lower() for word in tokens if word not in stop_words_set]
    sentence = rm_unimportant_words(words)
    
    return sentence


def stem(sentence):
    """
    Melakukan stemming untuk setiap kata yang ada pada teks

    Keyword arguments:
    sentences -- teks atau kalimat
    """ 

    stems = sentence

    tokens = word_tokenize(stems)
    stemmer = PorterStemmer()
    stems = [stemmer.stem(word) for word in tokens]

    stems = " ".join(stems)
    return stems

def preprocessing_docs(docs):
    """
    Melakukan praproses pada dokumen dengan menghilangkan stopwords,symbol,dan melakukan stemming

    Keyword arguments:
    docs -- list of document/text
    """ 

    processed_text = []
    for item in docs:
        temp = item.decode('utf-8')
        temp = rm_stopwords(temp)
        temp = stem(temp)
        temp = re.sub(r'[^\w\s]','',temp)
        processed_text.append(temp)

    return processed_text
    
def get_accuracy_fullmatch(label,predicted_df,row_training,offset):
    """
    Menghitung akurasi fullmatch

    Keyword arguments:
    label -- label target
    predicted_df -- dataframe hasil prediksi
    row_training -- batas baris training data
    offset -- offset
    """ 

    count = 0
    for i in range(len(predicted_df.values)):
        res = (label[row_training-offset:].values[i] == predicted_df[['food','price','service','ambience']].values[i])
        if(res[0]==True and res[1]==True and res[2]==True and res[3]==True):
            count+=1

    print("Akurasi Full-Match ", count*1.0/len(predicted_df.values))

def save_result_to_csv(df_test,predicted_df):
    """
    Menyimpan Output hasil prediksi dalam bentuk CSV

    Keyword arguments:
    df_test -- dataframe testing data
    predicted_df -- dataframe hasil prediksi 
    """ 

    submission_df = pd.concat( [df_test[['id','text']], predicted_df], axis=1 )
    DATASET_PATH = os.path.join("dataset", "testing_output.csv")
    submission_df.to_csv(DATASET_PATH)

