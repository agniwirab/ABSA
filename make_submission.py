"""
Module ini berfungsi untuk merubah hasil prediksi dalam bentuk CSV menjadi XML

"""

import pandas as pd
import os
import xml.etree.ElementTree as ET

PATH = os.path.dirname(os.path.abspath(__file__))
DATASET_PATH = os.path.join(PATH, "dataset", "testing_output.csv")
df = pd.read_csv(DATASET_PATH)

xml_str = '<corpus>\n'

aspects =  ['food','price','service','ambience']

#Process instances and Polarity
for i in range(len(df.values)):
    xml_str += '<review rid="'+ str(df['id'].values[i]) +'">\n'
    teks = str(df['text'].values[i])
    teks = teks.replace('<text>','')
    teks = teks.replace('</text>','')
    teks = teks.replace('&','&amp;')
    teks = teks.replace('<','&lt;')
    teks = teks.replace('>','&gt;')

    xml_str += "<text>"+  teks + "</text>\n"

    #if an instance has aspects, process the aspect and polarity
    if(any(v != 0 for v in df[aspects].values[i])):
        xml_str +='<aspects>\n'
        for aspect in aspects:
            if(int(df[aspect].values[i])!=0):
                polarity = df[aspect].values[i]
                if(polarity == 1):
                    polarity = 'POSITIVE'
                else:
                    polarity = 'NEGATIVE'

                xml_str+= '<aspect category="'+aspect.upper()+'" polarity="'+polarity+'"/>\n'

        xml_str +='</aspects>\n'
    xml_str += '</review>\n'


xml_str += '</corpus>'

#Write data to XML File
submission = open('submission.xml','w')
submission.write(xml_str)
submission.close
