"""
Program ini berfungsi untuk membuat wordcloud untuk mencari kata-kata yang tidak relevan

"""
import numpy as np 
import pandas as pd 
import matplotlib as mpl
import matplotlib.pyplot as plt
import os
import logging
from nltk.tokenize import word_tokenize
from subprocess import check_output
from wordcloud import WordCloud
from Sastrawi.StopWordRemover.StopWordRemoverFactory import StopWordRemoverFactory

mpl.rcParams['font.size']=13                
mpl.rcParams['savefig.dpi']=400             
mpl.rcParams['figure.subplot.bottom']=.1 

logging.basicConfig(level=logging.INFO)
factory = StopWordRemoverFactory()
stopword = factory.create_stop_word_remover()

DATASET_PATH = os.path.join("dataset", "training_set.csv")
df = pd.read_csv(DATASET_PATH)
sentences = ''.join(df['text'].values)
#sentences = stopword.remove(sentences)
sentences = sentences.decode('utf-8')
words = word_tokenize(sentences)
# listkata = ['untuk','ke','nya','yg','jadi','tapi','aja','deh','ini','juga','jg','dan','utk','krn','yang','untuk','jd','text','gue','gw',
# 'karna','di','ya','karna','aku','waktu','saya','kayak','coba','kalo','sama','kalau','emang','lg','lagi','tp','udah','dan','buat',
# 'amp','dll','blogspot','lol','ps','oh','man','hehe','haha','hahaha','hehehe']
# words = [word.lower() for word in words if word.lower() not in listkata]

sentences = ''.join(words)
# sentences = sentences.replace('nya','')
# sentences = sentences.replace(' bgt ',' banget ')
# sentences = sentences.replace('ambience','suasana')
# sentences = sentences.replace('decor','suasana')
# sentences = sentences.replace('interior','suasana')


wordcloud = WordCloud(
                          background_color='white',
                          max_words=500,
                          max_font_size=40, 
                          random_state=42
                         ).generate(sentences)

print(wordcloud)
fig = plt.figure(1)
plt.imshow(wordcloud)
plt.axis('off')
plt.show()