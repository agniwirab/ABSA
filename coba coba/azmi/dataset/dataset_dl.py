from bs4 import BeautifulSoup
import pandas as pd
import os
import sys
import argparse

parser = argparse.ArgumentParser()

group = parser.add_mutually_exclusive_group()
group.add_argument('-t', action='store_true', help="training dataset")
group.add_argument('-v', action='store_true', help="validation dataset")

args = parser.parse_args()

if args.t:
    DATASET_XML = "training_set.xml"
    DATASET_TSV = "training_dl.tsv"
elif args.v:
    DATASET_XML = "validation_set.xml"
    DATASET_TSV = "validation_dl.tsv"
else:
    print("choose dataset: -h")
    sys.exit()

PATH = os.path.dirname(os.path.abspath(__file__))

DATASET_PATH = os.path.join(PATH, DATASET_XML)

file_handler = open(DATASET_PATH, "r")
file_b = file_handler.read()
file_s = file_b.encode('utf-8')
file_handler.close()

xml = file_s
xml = BeautifulSoup(xml, "xml")
reviews = []


count = 0

for review in xml.corpus.findAll("review"):
    row = [0 for i in range(6)]
    rid = int(review["rid"])

    print(rid)

    try:
        teks = str(review.find("text").string)
    except:
        print(review.find("text"))

    aspect0 = {"FOOD": 0, "SERVICE": 0, "AMBIENCE": 0, "PRICE": 0}
    for aspect in review.find("aspects").findAll("aspect"):
        category = str(aspect["category"])
        polarity = str(aspect["polarity"])
        aspect0[category] = 1 if polarity == "POSITIVE" else 2

    row[0] = rid
    row[1] = teks
    row[2] = aspect0["FOOD"]
    row[3] = aspect0["PRICE"]
    row[4] = aspect0["SERVICE"]
    row[5] = aspect0["AMBIENCE"]

    reviews.append(row)

    aspect1 = {"FOOD": 0, "SERVICE": 0, "AMBIENCE": 0, "PRICE": 0}
    review1_available = review.find("aspects", {"id": "1"}) is not None
    if review1_available:
        for aspect in review.find("aspects", {"id": "1"}).findAll("aspect"):
            category = str(aspect["category"])
            polarity = str(aspect["polarity"])
            aspect1[category] = 1 if polarity == "POSITIVE" else 2

    if review1_available:
        row[0] = rid
        row[1] = teks
        row[2] = aspect1["FOOD"]
        row[3] = aspect1["PRICE"]
        row[4] = aspect1["SERVICE"]
        row[5] = aspect1["AMBIENCE"]

        reviews.append(row)


df = pd.DataFrame(reviews,
                  columns=['id', 'text', 'food', 'price', 'service', 'ambience'])

DATASET_PATH = os.path.join(PATH, DATASET_TSV)
df.to_csv(DATASET_PATH, sep="\t", index=False)

print(df.head())
