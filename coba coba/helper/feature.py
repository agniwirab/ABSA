import os
import string
import time
import numpy as np
from collections import Counter
from nltk.tokenize import word_tokenize
from nltk.stem import PorterStemmer
from nltk.corpus import stopwords as nltk_stopwords
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory
from Sastrawi.StopWordRemover.StopWordRemoverFactory import StopWordRemoverFactory

PATH = os.path.dirname(os.path.abspath(__file__))

rm_punctuation = str.maketrans('', '', string.punctuation)


def stopwords(sentence):
    factory = StopWordRemoverFactory()
    stopword = factory.create_stop_word_remover()
    words = stopword.remove(sentence)

    stop_words_set = set(nltk_stopwords.words('english'))
    tokens = word_tokenize(words)

    words = [word for word in tokens if word not in stop_words_set]

    words = " ".join(words)
    return words


def stem(sentence):
    # factory = StemmerFactory()
    # stemmer = factory.create_stemmer()
    # stems = stemmer.stem(sentence)
    stems = sentence

    tokens = word_tokenize(stems)
    stemmer = PorterStemmer()
    stems = [stemmer.stem(word) for word in tokens]

    stems = " ".join(stems)
    return stems


def tf_idf(docs, train_vocab=None, verbose=False):
    start = time.time()

    """ docs: list of sentence """
    if verbose:
        print("removing punctuation ...")
    docs = [sentence.translate(rm_punctuation).lower() for sentence in docs]
    if verbose:
        print("removing stopwords ...")
    docs = [stopwords(sentence) for sentence in docs]
    if verbose:
        print("stemming ...")
    docs = [stem(sentence) for sentence in docs]

    vocab = None
    if train_vocab:
        vocab = train_vocab
    else:
        vocab = [sentence.split() for sentence in docs]
        vocab = [word for words in vocab for word in words]
        vocab = dict(Counter(vocab))
        vocab = dict((word, cnt) for word, cnt in vocab.items() if cnt > 0)
        vocab = vocab.keys()
        vocab = dict((word, idx) for idx, word in enumerate(vocab))

    T = len(vocab)
    D = len(docs)

    tf = np.zeros((D, T))
    idf = np.ones(T)

    for idx, doc in enumerate(docs):
        if verbose and idx % 100 == 0:
            print("tf-idf", idx, "/", D)

        doc = doc.lower().translate(rm_punctuation).split()
        for term in doc:
            if term in vocab:
                tf[idx, vocab[term]] += 1
        for term in vocab.keys():
            if term in doc:
                idf[vocab[term]] += 1

    idf = D / idf
    idf = np.log(idf)

    tfidf = tf * idf

    l2 = np.square(tfidf)
    l2 = np.sum(l2, axis=1) + 1
    l2 = np.sqrt(l2)

    tfidf = np.transpose(np.transpose(tfidf) / l2)

    end = time.time()
    elapsed = end - start

    minutes = int(elapsed / 60)
    seconds = int(elapsed % 60)

    if verbose:
        print(minutes, "minutes", seconds, "seconds")

    return tfidf, vocab


# docs = ["The sky is blue.", "The sun is bright."]
# print(tf_idf(docs))
