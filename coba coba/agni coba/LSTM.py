import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
from keras.preprocessing.sequence import pad_sequences
from keras.preprocessing.text import Tokenizer
import matplotlib.pyplot as plt
import keras
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from keras.layers.embeddings import Embedding
import re
import os
import string
from sklearn import preprocessing
from nltk.corpus import stopwords
import nltk.data
from gensim.models import Word2Vec
import logging
import multiprocessing
import sys

### LOAD
PATH = os.path.dirname(os.path.abspath(__file__))
DATASET_PATH = os.path.join(PATH, "dataset", "training_set_gabung.csv")
df1 = pd.read_csv(DATASET_PATH)
DATASET_PATH = os.path.join(PATH, "dataset", "testing_set.csv")
#df2 = pd.read_csv(DATASET_PATH)
DATASET_PATH = os.path.join(PATH, "dataset", "validation_set.csv")
df3 = pd.read_csv(DATASET_PATH)


# extract the comments and labels 
list_classes = ["is_food","is_price","is_service","is_ambience"]
Y_train = df1[list_classes].values
Y_test = df3[list_classes].values
X_train_lst = df1["text"].values
X_test_lst  = df3["text"].values
X_test_ids  =  df3["id"].values
#set max dictionary size
dictionary_size = 20000
# tokenize the text 
tokenizer = Tokenizer(num_words=dictionary_size)
#create a dictionary index:word
tokenizer.fit_on_texts( X_train_lst )
#create Index representation of comments after tokenizing
X_train_tokenized_lst = tokenizer.texts_to_sequences(X_train_lst)
X_test_tokenized_lst  = tokenizer.texts_to_sequences(X_test_lst)

wordcount_per_comment = [len(comment) for comment in X_train_tokenized_lst]
#plt.hist(wordcount_per_comment, bins = np.arange(0,800,10))
#plt.show()


max_comment_length = 200
X_train = pad_sequences(X_train_tokenized_lst, maxlen=max_comment_length)
X_test =  pad_sequences(X_test_tokenized_lst, maxlen=max_comment_length )
#print(X_test[0])
# create model

#num_classes = 3
#Y_train = keras.utils.to_categorical(Y_train, num_classes)

embedding_vecor_length = 20
model = Sequential()
model.add(Embedding(dictionary_size, embedding_vecor_length, input_length=max_comment_length))
model.add(LSTM(150))
#model.add(Dense(50, activation='sigmoid'))
model.add(Dense(4, activation='softmax'))
model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
print(model.summary())

history = model.fit(X_train, Y_train, validation_split = 0.3, epochs=1, batch_size=64)

#prediction = model.predict(X_test[0])
#hasil = prediction.argmax(axis=1)

#print(X_test[0],hasil)
            
predictions = model.predict(X_test, verbose=1)
print( predictions[0:5])

df_hasil = pd.DataFrame(data=predictions, columns= list_classes )
df_hasil.head()
df_hasil_id = pd.DataFrame(X_test_ids, columns= ['id'] )
df_hasil_id.head()
label_test = pd.DataFrame(data=Y_test )
submission_df = pd.concat( [df_hasil_id, df_hasil,label_test], axis=1 )
print(submission_df)

