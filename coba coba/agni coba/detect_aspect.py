import nltk
from nltk.corpus import state_union, wordnet
from nltk.tokenize import word_tokenize
from nltk.stem import WordNetLemmatizer

import numpy as np
from textblob import TextBlob
from textblob.sentiments import NaiveBayesAnalyzer
import pandas as pd
import os
import numpy as np

global stemmer



def detect_food(NN_list):
    kamus = ['food','meat','drink','ice','taste','cream','mie','noodle','rice','water','tea','coffee','dessert']
    
    temp=[0]
    for item in NN_list:
        try:
            wordFromList1 = wordnet.synsets(item)
            for food in kamus:
                wordFromList2 = wordnet.synsets(food)
                try:
                    sim_value = wordFromList1[0].path_similarity(wordFromList2[0])
                    temp.append(sim_value)
                except:
                    continue
                
                if(food in item):
                    temp.append(1)
        except:
            continue

    return max(temp)>=0.25
        
def detect_price(NN_list,ADJ_list):
    kamus = ['price','portion','size','expensive','cheap','afford']
    
    temp=[0]
    for item in NN_list:
        try:
            wordFromList1 = wordnet.synsets(item)
            for food in kamus:
                wordFromList2 = wordnet.synsets(food)
                try:
                    sim_value = wordFromList1[0].path_similarity(wordFromList2[0])
                    temp.append(sim_value)
                except:
                    continue
        except:
            continue

    for item in ADJ_list:
        try:
            wordFromList1 = wordnet.synsets(item)
            for price in kamus:
                wordFromList2 = wordnet.synsets(food)
                try:
                    sim_value = wordFromList1[0].path_similarity(wordFromList2[0])
                    temp.append(sim_value)
                except:
                    continue
        except:
            continue

    return max(temp)>=0.5

def detect_service(NN_list,ADJ_list):
    kamus = ['wait','service']
    
    temp=[0]
    for item in NN_list:
        try:
            wordFromList1 = wordnet.synsets(item)
            for food in kamus:
                wordFromList2 = wordnet.synsets(food)
                try:
                    sim_value = wordFromList1[0].path_similarity(wordFromList2[0])
                    temp.append(sim_value)
                except:
                    continue
        except:
            continue
    
    for item in ADJ_list:
        try:
            wordFromList1 = wordnet.synsets(item)
            for food in kamus:
                wordFromList2 = wordnet.synsets(food)
                try:
                    sim_value = wordFromList1[0].path_similarity(wordFromList2[0])
                    temp.append(sim_value)
                except:
                    continue
        except:
            continue


    return max(temp)>=0.5

def detect_ambience(NN_list,ADJ_list):
    kamus = ['decor','cozy','interior','concept', "area",'atmosphere']
    
    temp=[0]
    for item in NN_list:
        try:
            wordFromList1 = wordnet.synsets(item)
            for food in kamus:
                wordFromList2 = wordnet.synsets(food)
                try:
                    sim_value = wordFromList1[0].path_similarity(wordFromList2[0])
                    temp.append(sim_value)
                except:
                    continue
        except:
            continue
    
    for item in ADJ_list:
        try:
            wordFromList1 = wordnet.synsets(item)
            for food in kamus:
                wordFromList2 = wordnet.synsets(food)
                try:
                    sim_value = wordFromList1[0].path_similarity(wordFromList2[0])
                    temp.append(sim_value)
                except:
                    continue
        except:
            continue

    return max(temp)>=0.5
        
       
            


def aspect_extraction(input_text):
    aspects = ["food","price","service","ambience"]
    try:
        text = word_tokenize(input_text)
    except:
        text = "gatot"
        
    postag = nltk.pos_tag(text)
    #print postag
    list_NN = [word for (word,tag) in  postag if tag=='NN' or tag=='NNP' or tag=='NNS']
    list_ADJ = [word for (word,tag) in  postag if tag=='JJ' or tag=='VBG']

    #aspect_ADJ = extract_aspect_ADJ(list_ADJ,aspects)
    result = {}
    if detect_food(list_NN):
        result['FOOD'] = 0

    if detect_price(list_NN,list_ADJ):
        result['PRICE'] = 0

    if detect_service(list_NN,list_ADJ):
        result['SERVICE'] = 0

    if detect_ambience(list_NN,list_ADJ):
        result['AMBIENCE'] = 0

    return result
    
    
#input_text = "We've  tried  miitem  with  tuna  rica2,  miitem  aglio  olio,  miitem  with  shrimp  n  chilli, miitem with  creamy  n  shrimp...  i  highly  recommend  miitem  with  tuna  rica2... it tastes so delicious..."
#input_text = "Overall I was very pleased with Ryanair. The price I paid was really cheap in comparison to the others on offer and as someone looking to save money"
#input_text = "Best place to date someone. Good ambiance nice interior. Decent price (used to be)  Best hamburger but my favorite are their alfredo or carbonara  Prince house seharusnya saya rate 5.0  Its just their affordable waffle no longer there, the price is not worth anymore."



PATH = os.path.dirname(os.path.abspath(__file__))
DATASET_PATH = os.path.join(PATH, "dataset", "training_set.csv")
df = pd.read_csv(DATASET_PATH)

teks= ((df['text']).values)
training_labels = df[['is_food','is_price','is_service','is_ambiance']].values

aspect_dict = {}
predicted = []

for item in teks:
    row = [0,0,0,0]
    aspects = aspect_extraction(str(item))
    for key in aspects:
        if(key=='FOOD'):
            row[0]=1
        elif key=='PRICE':
            row[1]=1
        elif key=='SERVICE':
            row[2]=1
        else:
            row[3]=1
    
    predicted.append(row)
    
print(np.mean(predicted == training_labels),'isfood')