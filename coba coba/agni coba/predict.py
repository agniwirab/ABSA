import pandas as pd
import re
import os
import numpy as np
import string
import sys
from sklearn import preprocessing
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn.linear_model import LogisticRegression
from sklearn import svm
from gensim import parsing
from sklearn.cross_validation import train_test_split
from sklearn import model_selection
from sklearn.model_selection import cross_val_score
from sklearn.feature_selection import SelectFromModel
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import f_regression, f_classif
from sklearn.feature_selection import VarianceThreshold
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.pipeline import Pipeline
from nltk.tokenize import word_tokenize
from nltk.stem import PorterStemmer
from nltk.corpus import stopwords as nltk_stopwords
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory
from Sastrawi.StopWordRemover.StopWordRemoverFactory import StopWordRemoverFactory
from sklearn.neural_network import MLPClassifier
from scipy.sparse import coo_matrix, hstack, csr_matrix



PATH = os.path.dirname(os.path.abspath(__file__))
DATASET_PATH = os.path.join(PATH, "dataset", "training_set_gabung.csv")
df = pd.read_csv(DATASET_PATH)

DATASET_PATH = os.path.join(PATH, "dataset", "validation_set.csv")
df_test = pd.read_csv(DATASET_PATH)
transformer = TfidfTransformer(smooth_idf=False)
count_vect = CountVectorizer(lowercase=True,stop_words="english",ngram_range=(1,3))
rm_punctuation = string.maketrans('', '')

row_training,col_training = df.shape
print(df.shape)
df = pd.concat([df,df_test])

def stopwords(sentence):
    factory = StopWordRemoverFactory()
    stopword = factory.create_stop_word_remover()
    words = stopword.remove(sentence)

    stop_words_set = set(nltk_stopwords.words('english'))
    tokens = word_tokenize(words)

    not_important_words = ['kak','untuk','ke','nya','yg','jadi','tapi','aja','deh','ini','juga','jg','dan','utk','krn','yang','untuk','jd','text','gue','gw',
'karna','di','ya','karna','aku','waktu','saya','kayak','coba','kalo','sama','kalau','emang','lg','lagi','tp','udah','dan','buat','pa','li',
'amp','dll','blogspot','lol','ps','oh','man','hehe','haha','hahaha','hehehe']

    words = [word.lower() for word in tokens if word not in stop_words_set]
    
    words = [word for word in words if word not in not_important_words]

    
        
    words = " ".join(words)
    words = words.replace('nya','')
    words = words.replace(' bgt ',' banget ')
    words = words.replace('ambience','suasana')
    words = words.replace('decor','suasana')
    words = words.replace('interior','suasana')
    
    return words


def stem(sentence):
    # factory = StemmerFactory()
    # stemmer = factory.create_stemmer()
    # stems = stemmer.stem(sentence)
    stems = sentence

    tokens = word_tokenize(stems)
    stemmer = PorterStemmer()
    stems = [stemmer.stem(word) for word in tokens]

    stems = " ".join(stems)
    return stems



teks= ((df['text']).values)
processed_text = []
for item in teks:
    temp = item.decode('utf-8')
    temp = stopwords(temp)
    temp = stem(temp)
    temp = re.sub(r'[^\w\s]','',temp)
    processed_text.append(temp)
    


X = count_vect.fit_transform(processed_text)
#X = preprocessing.scale(X,with_mean=False)
#X= preprocessing.normalize(X, norm='l2')

#X_tfidf = transformer.fit_transform(X)
#X = csr_matrix(hstack([X,X_tfidf]))

offset = int(sys.argv[1])
predicted = []
for i in ['is_food','is_price','is_service','is_ambience']:
    
    y=df[i]
    

    logreg = LogisticRegression()
    anova_filter = SelectKBest(f_classif, k=300)
    clf = Pipeline([
    ('anova', anova_filter),
    ('classification', LogisticRegression(penalty='l1')),
    ])

    clf.fit(X[:row_training-offset], y[:row_training-offset])
    y_pred = clf.predict(X[row_training-offset:])
    predicted.append(y_pred)
    print('Accuracy of on validation set: {:.2f}'.format(clf.score(X[row_training-offset:], y[row_training-offset:])),i)

print predicted[0][:5],predicted[1][:5],predicted[2][:5],predicted[3][:5]
label = df[['is_food','is_price','is_service','is_ambience']]
predicted = pd.DataFrame({'v1_food': predicted[0].ravel(), 'v2_price': predicted[1].ravel(), 'v3_service': predicted[2].ravel(), 'v4_ambience': predicted[3].ravel()})
print predicted.head()



count = 0

for i in range(len(predicted.values)):
    res = (label[row_training-offset:].values[i] == predicted.values[i])
    #print(label[row_training-offset:].values[i],predicted.values[i])
    if(res[0]==True and res[1]==True and res[2]==True and res[3]==True):
        count+=1

print(len(predicted.values))
print(count)
print(count*1.0/len(predicted.values),count,len(predicted.values))
