"""
Module ini berfungsi merubah bentuk training data menjadi CSV
Module ini juga dapat untuk merubah bentuk testing data dengan berberapa modifikasi

"""

from bs4 import BeautifulSoup
import pandas as pd
import os

PATH = os.path.dirname(os.path.abspath(__file__))

DATASET_PATH = os.path.join(PATH, "dataset", "training_set.xml")
print(DATASET_PATH)

file_handler = open(DATASET_PATH, "r")
file_b = file_handler.read()
file_s = file_b.decode('utf-8')
file_handler.close()

xml = file_s
xml = BeautifulSoup(xml, "xml")
reviews = []

count = 0
for review in xml.corpus.findAll("review"):
    row = [0 for i in range(6)]
    rid = int(review["rid"])

    count+=1
    
    try:
        teks = str(review.find("text").string)
    except:
        teks = (review.find("text"))

    
    try:
        aspect0 = {"FOOD": 0, "SERVICE": 0, "AMBIENCE": 0, "PRICE": 0}
        for aspect in review.find("aspects", {"id": "0"}).findAll("aspect"):
            category = str(aspect["category"])
            polarity = str(aspect["polarity"])
            aspect0[category] = 1 if polarity == "POSITIVE" else 2

        aspect1 = {"FOOD": 0, "SERVICE": 0, "AMBIENCE": 0, "PRICE": 0}
        if review.find("aspects", {"id": "1"}) == None:
            aspect1 = aspect0
        else:
            for aspect in review.find("aspects", {"id": "1"}).findAll("aspect"):
                category = str(aspect["category"])
                polarity = str(aspect["polarity"])
                aspect1[category] = 1 if polarity == "POSITIVE" else 2

        for key in aspect0:
            if key == "FOOD":
                cat = 2
                pol = 2
            elif key == "SERVICE":
                cat = 4
                pol = 4
            elif key == "PRICE":
                cat = 3
                pol = 3
            else:
                cat = 5
                pol = 5

            #check polarity between set aspect 1 and set aspect 0
            # 2 = NEGATIVE
            # 1 = POSITIVE
            # 2 = NONE
            if (aspect0[key] == 1 and aspect1[key] == 2) or (aspect0[key] == 2 and aspect1[key] == 1):
                row[pol] = 2
            elif aspect0[key] == 1 or aspect1[key] == 1:
                row[pol] = 1
            elif aspect0[key] == 2 and aspect1[key] == 2:
                row[pol] = 2
            else:
                row[pol] = 0

    except Exception as e:
        print(e)
        #if instance doesnt has aspect, set [0,0,0,0] 
        row[2:] = [0,0,0,0]

    row[0] = (rid)
    row[1] = (teks)

    reviews.append(row)

df = pd.DataFrame(reviews, columns=['id', 'text', 'food', 'price', 'service',
                                    'ambience'])

DATASET_PATH = os.path.join(PATH, "dataset", "training_set_test.csv")
df.to_csv(DATASET_PATH)

print(df.head())
