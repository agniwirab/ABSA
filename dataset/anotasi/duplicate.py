from bs4 import BeautifulSoup

file_handler = open("dataset.xml", "r")
xml = file_handler.read()
file_handler.close()

xml = BeautifulSoup(xml, "xml")

rids = []
dupl = []

for review in xml.corpus.findAll("review"):
    rid = int(review["rid"])
    if rid in rids:
        dupl.append(rid)
    else:
        rids.append(rid)

# dupl.sort()

for idx, rid in enumerate(dupl, 1):
    rid = str(rid)
    print(idx, rid)

    reviews = xml.corpus.findAll("review", {"rid": rid})
    review1 = reviews[0]
    review2 = reviews[1]

    name = "azmi"
    categories = ["FOOD", "PRICE", "SERVICE", "AMBIENCE"]

    for category in categories:
        aspect1 = review1.find("aspect", {"name": name, "category": category})
        aspect2 = review1.find("aspect", {"name": name, "category": category})

        if aspect1["polarity"] == "":
            aspect1["polarity"] = aspect2["polarity"]
        elif aspect2["polarity"] == "":
            aspect2["polarity"] = aspect1["polarity"]

file_handler = open("dataset_new.xml", "w")
file_handler.write(xml.prettify())
file_handler.close()
