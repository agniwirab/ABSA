from bs4 import BeautifulSoup
import pandas as pd
import os
import sys
import argparse

parser = argparse.ArgumentParser()

group = parser.add_mutually_exclusive_group()
group.add_argument('-t', action='store_true', help="training dataset")
group.add_argument('-v', action='store_true', help="validation dataset")

args = parser.parse_args()

if args.t:
    DATASET = "training_set"
elif args.v:
    DATASET = "validation_set"
else:
    print("choose dataset: -h")
    sys.exit()

PATH = os.path.dirname(os.path.abspath(__file__))

DATASET_PATH = os.path.join(PATH, DATASET + ".xml")

file_handler = open(DATASET_PATH, "r")
file_b = file_handler.read()
file_s = file_b.encode('utf-8')
file_handler.close()

xml = file_s
xml = BeautifulSoup(xml, "xml")
reviews = []


count = 0

for review in xml.corpus.findAll("review"):
    row = [0 for i in range(10)]
    rid = int(review["rid"])

    print(rid)

    try:
        teks = str(review.find("text").string)
    except:
        print(review.find("text"))

    aspect0 = {"FOOD": 0, "SERVICE": 0, "AMBIENCE": 0, "PRICE": 0}
    for aspect in review.find("aspects").findAll("aspect"):
        category = str(aspect["category"])
        polarity = str(aspect["polarity"])
        aspect0[category] = 1 if polarity == "POSITIVE" else -1

    aspect1 = {"FOOD": 0, "SERVICE": 0, "AMBIENCE": 0, "PRICE": 0}
    if review.find("aspects", {"id": "1"}) == None:
        aspect1 = aspect0
    else:
        for aspect in review.find("aspects", {"id": "1"}).findAll("aspect"):
            category = str(aspect["category"])
            polarity = str(aspect["polarity"])
            aspect1[category] = 1 if polarity == "POSITIVE" else -1

    for key in aspect0:
        if key == "FOOD":
            cat = 2
            pol = 6
        elif key == "SERVICE":
            cat = 4
            pol = 8
        elif key == "PRICE":
            cat = 3
            pol = 7
        else:
            cat = 5
            pol = 9

        if aspect0[key] != 0 and aspect1[key] != 0:
            row[cat] = 1
        elif aspect0[key] != 0 or aspect1[key] != 0:
            row[cat] = 0.5
        else:
            row[cat] = 0

        if (aspect0[key] == 1 and aspect1[key] == -1) or (aspect0[key] == -1 and aspect1[key] == 1):
            row[pol] = 0.5
        elif aspect0[key] == 1 or aspect1[key] == 1:
            row[pol] = 1
        else:
            row[pol] = 0

    row[0] = (rid)
    row[1] = (teks)

    reviews.append(row)


df = pd.DataFrame(reviews, columns=['id', 'text', 'is_food', 'is_price', 'is_service',
                                    'is_ambiance', 'food_polarity', 'price_polarity', 'service_polarity', 'ambiance_polarity'])

DATASET_PATH = os.path.join(PATH, DATASET + ".tsv")
df.to_csv(DATASET_PATH, sep="\t", index=False)

print(df.head())
