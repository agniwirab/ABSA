"""
File ini merupakan Main Program untuk proyek akhir NLP

"""

import pandas as pd
import re
import os
import numpy as np
import string
import sys
from sklearn import preprocessing, model_selection
from sklearn.feature_extraction.text import TfidfTransformer, CountVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.feature_selection import SelectFromModel, SelectKBest, f_classif, f_regression
from sklearn.pipeline import Pipeline
from sklearn.metrics import classification_report
from utils import rm_stopwords, get_accuracy_fullmatch, save_result_to_csv, stem, preprocessing_docs

    
if __name__ == '__main__':

    # Read training data
    PATH = os.path.dirname(os.path.abspath(__file__))
    DATASET_PATH = os.path.join(PATH, "dataset", "training_set.csv")
    df = pd.read_csv(DATASET_PATH)

    # Read validation set
    DATASET_PATH = os.path.join(PATH, "dataset", "gold_set.csv")
    df_test = pd.read_csv(DATASET_PATH)



    row_training,col_training = df.shape
    df = pd.concat([df,df_test])
    docs = ((df['text']).values)
    
    # Praproses dokumen
    processed_docs = preprocessing_docs(docs)

    # Membuat bag of words
    count_vect = CountVectorizer(lowercase=True,stop_words="english",ngram_range=(1,3))
    X = count_vect.fit_transform(processed_docs)


    offset = 0
    list_classes = ['food','price','service','ambience']
    predicted = []

    # Feature selection
    anova_filter = SelectKBest(f_classif, k=300)

    # create pipeline
    clf = Pipeline([('anova', anova_filter),('classification', LogisticRegression(penalty='l1'))])

    for i in list_classes:
        
        y=df[i]
        #proses training untuk suatu aspek/target y
        clf.fit(X[:row_training-offset], y[:row_training-offset])
        #proses prediksi
        y_pred = clf.predict(X[row_training-offset:])
        predicted.append(y_pred)

        
        print("#"*20,i,"#"*20)
        print('Accuracy of on validation set: {:.2f}'.format(clf.score(X[row_training-offset:], y[row_training-offset:])),i)
        print(classification_report(y[row_training-offset:], y_pred, target_names=['NONE','POSITIVE','NEGATIVE']))
        print("#"*50)
        print("\n")


    label = df[['food','price','service','ambience']]
    predicted_df = pd.DataFrame({'food': predicted[0].ravel(), 'price': predicted[1].ravel(), 'service': predicted[2].ravel(), 'ambience': predicted[3].ravel()})

    get_accuracy_fullmatch(label,predicted_df,row_training,offset)
    #save_result_to_csv(df_test,predicted_df)

